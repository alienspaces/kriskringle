# Kris Kringle

Basic Ruby on Rails project for family Kris Kringle.

## NOTE

Was thrown together quite quickly and is no longer maintained.

You *could* maybe do something useful with this but that is entirely at your own risk :)

## Setup

Make sure you have ruby 2.5.1, rails 5.2 and bundler installed.

```bash
bundle install --path ./vendor/bundle
```

## Run

```bash
bundle exec rails server

http://localhost:3000/
```
