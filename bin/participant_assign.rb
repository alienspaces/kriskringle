#!/usr/bin/env ruby

require "sqlite3"

# Open a database
@db = nil
if ENV['RAILS_ENV'] == 'production'
    p "Assigning production kriskringles"
    @db = SQLite3::Database.new "db/production.sqlite3"
else    
    p "Assigning production kriskringles"
    @db = SQLite3::Database.new "db/development.sqlite3"
end

# participants
@participants = @db.execute( "select * from participants" )

# participant excludes
@participant_excludes = @db.execute( "select * from participant_excludes" )

# participant assign
@participant_assign = {}

# reset assigned
def reset_assigned
  @participants.each {|participant|
    @participant_assign[participant[0]]= nil
  }
end

# all assigned
def all_assigned
  @participant_assign.each { |k, v|
    if @participant_assign[k] == nil
      return false
    end
  }
  true
end

# assign
def assign

  # reset assigned
  reset_assigned()

  try = 0
  max_try = 10
  while !all_assigned()

    try += 1
    if try == max_try
      break
    end

    # reset assigned
    reset_assigned()

    p ""
    p "[***] Trying #{try} time"
    p ""
    @participants.each { |participant|
      p "[***] Finding assign for participant id #{participant[0]} #{participant[1]}"
      @available = []
      @participants.each { |possible|
        # is me
        if possible[0] == participant[0]
          p "   [***] Found self, excluding participant id #{possible[0]}"
          next
        end

        # is excluded
        found_exclude = false
        @participant_excludes.each { |participant_exclude|
          if (participant_exclude[1] == participant[0]) && (participant_exclude[2] == possible[0])
            p "   [***] Found exclude, excluding participant id #{participant_exclude[2]}"
            found_exclude = true
            break
          end
        }
        if found_exclude
          next
        end

        # is assigned
        found_assigned = false;
        @participant_assign.each { |k, v|
          if possible[0] == v
            p "   [***] Found assigned, excluding participant id #{v}"
            found_assigned = true
            break
          end
        }
        if found_assigned
          next
        end

        # available
        @available << possible
      }

      if @available.length == 0
        break
      end

      random = rand(@available.length)
      @participant_assign[participant[0]] = @available[random][0]
      p "[***] Assigned #{@available[random][1]} to #{participant[1]}"
      p ""
    }

    p "[***] All assigned #{@participant_assign}"
  end

end

# delete assigned
def delete_assigned
  @db.execute( "delete from participant_assigns" )
end

def save_assigned
  @participant_assign.each { |k, v|
    assignee = nil
    assigned = nil
    @participants.each { |participant|
      if participant[0] == k
        assignee = participant
      end
      if participant[0] == v
        assigned = participant
      end
    }
    p "#{assignee[1]} has #{assigned[1]}"
    @db.execute("insert into participant_assigns (participant_id, assign_participant_id, created_at, updated_at) values (#{k}, #{v}, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)")
  }
end

# delete assigned
delete_assigned()

# assign
assign()

if (!all_assigned())
  p "Failed to assign everyone :("
  exit
end

# save assigned
save_assigned()
