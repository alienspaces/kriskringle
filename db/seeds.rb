# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# kim and ben
@participant_kim = Participant.create(
  name: 'Kim Tran', email: 'knhat_tran@yahoo.com', enabled: 1, admin: 0
)

@participant_ben = Participant.create(
  name: 'Ben Wallin', email: 'alienspaces@gmail.com', enabled: 1, admin: 1
)

# phuong
@participant_phuong = Participant.create(
  name: 'Phuong Tran', email: 'catphuong186@gmail.com', enabled: 1, admin: 0
)

# lachlan
@participant_lachlan = Participant.create(
  name: 'Lachlan Wallin', email: 'lachlan_wallin@yahoo.com', enabled: 1, admin: 0
)

# michelle and jonothan
@participant_michelle = Participant.create(
  name: 'Michelle Mills', email: 'jonmich@tpg.com.au', enabled: 1, admin: 0
)

@participant_jonothan = Participant.create(
  name: 'Jonathan Mills', email: 'jonmich@tpg.com.au', enabled: 1, admin: 0
)

# mum and dad
@participant_garry = Participant.create(
  name: 'Garry Wallin', email: 'garry.gayle@gmail.com', enabled: 1, admin: 0
)

@participant_gayle = Participant.create(
  name: 'Gayle Wallin', email: 'garry.gayle@gmail.com', enabled: 1, admin: 0
)

# dinh and anh
@participant_dinh = Participant.create(
  name: 'Dinh Tran', email: 'dinhkimtran1992@gmail.com', enabled: 1, admin: 0
)

@participant_anh = Participant.create(
  name: 'Anh Dinh', email: 'dinhkimtran1992@gmail.com', enabled: 1, admin: 0
)

# Excludes:
#   Ben and Kim
ParticipantExclude.create(participant_id: @participant_kim.id, exclude_participant_id: @participant_ben.id)
ParticipantExclude.create(participant_id: @participant_ben.id, exclude_participant_id: @participant_kim.id)

# Excludes:
#   Michelle and Jonothan
ParticipantExclude.create(participant_id: @participant_michelle.id, exclude_participant_id: @participant_jonothan.id)
ParticipantExclude.create(participant_id: @participant_jonothan.id, exclude_participant_id: @participant_michelle.id)

# Excludes:
#   Mum and Dad
ParticipantExclude.create(participant_id: @participant_garry.id, exclude_participant_id: @participant_gayle.id)
ParticipantExclude.create(participant_id: @participant_gayle.id, exclude_participant_id: @participant_garry.id)

# Excludes:
#   Dinh and Anh
ParticipantExclude.create(participant_id: @participant_dinh.id, exclude_participant_id: @participant_anh.id)
ParticipantExclude.create(participant_id: @participant_anh.id, exclude_participant_id: @participant_dinh.id)
