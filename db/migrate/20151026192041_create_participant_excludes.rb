class CreateParticipantExcludes < ActiveRecord::Migration[5.2]
  def change
    create_table :participant_excludes do |t|
      t.references :participant, index: true, foreign_key: true
      t.integer :exclude_participant_id

      t.timestamps
    end
  end
end
