class CreateParticipantWishes < ActiveRecord::Migration[5.2]
  def change
    create_table :participant_wishes do |t|
      t.text :description
      t.references :participant, index: true, foreign_key: true

      t.timestamps
    end
  end
end
