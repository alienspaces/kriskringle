class CreateParticipantAssigns < ActiveRecord::Migration[5.2]
  def change
    create_table :participant_assigns do |t|
      t.references :participant, index: true, foreign_key: true
      t.integer :assign_participant_id

      t.timestamps
    end
  end
end
