require 'digest/sha1'
require 'date'

class KrisKringleMailer < ApplicationMailer
  def kriskringle_assigned_email(participant, participant_assign)
    @participant = participant
    @participant_assign = participant_assign
    @digest = Digest::SHA1.hexdigest(@participant.name + @participant.email)

    Rails.logger.debug("Mailer digest is #{@digest}")

    date_now = Date.today
    date_christmas = Date.new(Date.today.year, 12, 25)
    difference_in_days = (date_christmas - date_now).to_i

    subject= "Kriskringle #{Time.now.strftime('%Y')} - " + @participant.name.partition(" ")[0] + " - Only #{difference_in_days} days until Christmas!"

    mail(:to => @participant.email, :subject => subject)

  end
end
