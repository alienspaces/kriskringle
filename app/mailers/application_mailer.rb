class ApplicationMailer < ActionMailer::Base
  default from: Settings.mailers.from_address
  layout 'mailer'
end
