class Participant < ActiveRecord::Base
  has_many :participant_wishes
  has_one :participant_assigns
end
