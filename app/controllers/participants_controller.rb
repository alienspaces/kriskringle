require 'uri'

class ParticipantsController < ApplicationController
  def index
    @participants = Participant.all

    # allow only for admin
    if (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id} and not your kriskringle, redirecting")
      redirect_to :root
      return
    end

  end

  def show
    @participant = Participant.find(params[:id])

    # allow only if this is you or your kriskringle
    if (@participant.id != @participant_you.id) && (@participant.id != @participant_kriskringle.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id} and not your kriskringle, redirecting")
      redirect_to :root
      return
    end

    @participant_wishes_links = {}
    @participant.participant_wishes.each { |pw|

      pw.description.sub!(/\<.*\>/, '')

      while pw.description.match(/(http|https|www\.)(:\/\/)?[\S]+/) { |m|
          pw.description.sub!(/(http|https|www\.)(:\/\/)?[\S]+/, '')
          if @participant_wishes_links[pw.id.to_s].nil?
            @participant_wishes_links[pw.id.to_s] = []
          end
          m_text = m.to_s
          m_text.sub!(/^www/, 'http://www')
          @participant_wishes_links[pw.id.to_s] << m_text
        }

        Rails.logger.debug("Have participant wishes links #{@participant_wishes_links[pw.id.to_s]}")
      end
    }
    Rails.logger.debug("Have all participant wishes links #{@participant_wishes_links}")

  end

  def email
    Rails.logger.debug("Emailing participants")

    set_participant_you()
    check_participant_you()
    set_participant_kriskringle()

    # allow only if you are an admin
    if (!@participant_you || !@participant_you.admin)
      Rails.logger.debug("Participant is null or not and admin, redirecting")
      redirect_to :root
      return
    end

    params[:participants].each { |participant|

      if params[:participants][participant][:email].to_i == 1

        participant = Participant.find(participant)

        Rails.logger.debug("Sending email to participant #{participant.name}")

        assigned_participant = ParticipantAssign.find_by participant_id: participant.id
        if assigned_participant
          participant_assign = Participant.find_by id: assigned_participant.assign_participant_id
        end

        KrisKringleMailer.kriskringle_assigned_email(participant, participant_assign).deliver_later(queue: 'mailers')
      end
    }

    redirect_to participants_path
    return
  end

end
