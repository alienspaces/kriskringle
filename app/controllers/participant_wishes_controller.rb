class ParticipantWishesController < ApplicationController
  before_action :get_wish_hint, only: [:edit, :new]

  def index
    @participant_wishes = ParticipantWish.find(params[:participant_id])
  end

  def show
    @participant = Participant.find(params[:participant_id])

    # allow only if this is you or your kriskringle
    if (@participant.id != @participant_you.id) && (@participant.id != @participant_kriskringle.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id} and not your kriskringle, redirecting")
      redirect_to :root
      return
    end

    @participant_wish = ParticipantWish.find(params[:id])
  end

  def new
    @participant = Participant.find(params[:participant_id])

    # allow only if this is you
    if (@participant.id != @participant_you.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id}, redirecting")
      redirect_to :root
      return
    end
  end

  def create
    @participant = Participant.find(params[:participant_id])

    # allow only if this is you
    if (@participant.id != @participant_you.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id}, redirecting")
      redirect_to :root
      return
    end

    @participant_wish = @participant.participant_wishes.create(participant_wish_params)

    redirect_to participant_path(@participant)
  end

  def edit
    @participant = Participant.find(params[:participant_id])

    # allow only if this is you
    if (@participant.id != @participant_you.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id}, redirecting")
      redirect_to :root
      return
    end

    @participant_wish = @participant.participant_wishes.find(params[:id])
  end

  def update
    @participant = Participant.find(params[:participant_id])

    # allow only if this is you
    if (@participant.id != @participant_you.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id}, redirecting")
      redirect_to :root
      return
    end

    @participant_wish = @participant.participant_wishes.find(params[:id])
    if @participant_wish.update(participant_wish_params)
      redirect_to @participant
    else
      render 'edit'
    end
  end

  def destroy
    @participant = Participant.find(params[:participant_id])

    # allow only if this is you
    if (@participant.id != @participant_you.id) && (!@participant_you.admin)
      Rails.logger.debug("Participant id #{@participant.id} is not you #{@participant_you.id}, redirecting")
      redirect_to :root
      return
    end

    @participant_wish = @participant.participant_wishes.find(params[:id])
    @participant_wish.destroy

    redirect_to participant_path(@participant)
  end

  private

  def participant_wish_params
    params.require(:participant_wish).permit(:description)
  end
end
