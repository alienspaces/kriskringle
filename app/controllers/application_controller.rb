require 'digest/sha1'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_participant_you
  before_action :check_participant_you
  before_action :set_participant_kriskringle

  # you
  @participant_you = nil

  # your kris kringle
  @participant_kriskringle = nil

  def get_wish_hint
    hints = [
      'If you can find your wish online add a web link to your wish to make it easier to find your gift',
      'If your wish is a gift of clothing make sure you include your size in the description',
      'If your wish comes in different colours make sure you include your colour preference'
    ];

    random_hint_number = rand(hints.length);

    Rails.logger.debug("Ranom hint number is #{random_hint_number}")

    @hint = hints[random_hint_number]

    Rails.logger.debug("Have hint #{@hint}")

  end

  def set_participant_you
    # all participants
    @participants = Participant.all;

    @participants.each { |p|
      @digest = Digest::SHA1.hexdigest(p.name + p.email)
      Rails.logger.debug("Have participant #{p.name} #{p.email} digest #{@digest}")
      if params[:key] && params[:key] == @digest
        @participant_you = p
        cookies[:digest] = @digest
        Rails.logger.debug("You are participant #{p.name}")
      elsif cookies[:digest] == @digest
        @participant_you = p
        Rails.logger.debug("You are already participant #{p.name}")
      end
    }
  end

  def set_participant_kriskringle
    if @participant_you
      Rails.logger.debug("Finding assigned Kriskringle")
      assigned_participant = ParticipantAssign.find_by participant_id: @participant_you.id
      if assigned_participant
        @participant_kriskringle = Participant.find_by id: assigned_participant.assign_participant_id
        Rails.logger.debug("You have participant kriskringle #{@participant_kriskringle.name}")
      end
    end
  end

  def check_participant_you

    Rails.logger.debug("Have cookie digest #{cookies[:digest]}")

    if (!cookies[:digest])
      Rails.logger.debug("Missing cookie digest, redirecting")
      redirect_to :root
    end
  end

end
