Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'

  get '/participants', to: 'participants#index'
  get '/participants/:id', to: 'participants#show', as: 'participant'
  post '/participants/email', to: 'participants#email'
  resources :participants do
    resources :participant_wishes
  end
end
